# Agar.io Clone
A clone of the once popular online game agar.io. Intended to be peer-to-peer multiplayer in the future. Work in progress.

## Usage
Run `npx serve` inside the `public` directory.

## Roadmap
- Multiplayer
- Performance Improvements
- Smart Bots