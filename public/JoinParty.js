


export default function JoinParty() {

    let gameState = {};
    let conn;

    let otherId = prompt("Other ID:");
    if (!otherId) {
        welcomeContainer.style.display = "block";
        return;
    }
    let peer = new Peer();

    let joined = false;
    let view;

    peer.on("open", (id) => {
        conn = peer.connect(otherId);
        conn.on('open', () => {

            conn.send({ "type": "join_request" });




        });

        conn.on("data", (response) => {



            if (response.type == "peer_update") {
                if (!joined) return;
                let data = response.data;
                gameState.players = data.players;
                gameState.foodCollection.addQueue.push(...data.addedFood);
                gameState.foodCollection.removeQueue.push(...data.removedFood);
            }

            if (response.type == "join_reply") {
                console.log("JOIN REPLY")
                if (response.joinDenied) {
                    console.log("JOIN DENIED");
                    return;
                }
                joined = true;

                welcomeContainer.style.display = "none";

                gameState = response.data.initialData;

                gameState.foodCollection = new FoodCollectionView(gameState.foodCollection);

                let playerId = response.data.playerId;

                view = new SVGView(gameState, playerId, () => {
                    // SPLIT

                });


                window.requestAnimationFrame(function viewStep(timestamp) {
                    console.log("AS")
                    conn.send({
                        type: "mouse_position_update",
                        playerId: response.data.playerId,
                        position: view.ingameMousePosition
                    });

                    view.updateView();
                    window.requestAnimationFrame(viewStep);
                });

            }


        });
    });




}


JoinParty.prototype.stop = function () {

}