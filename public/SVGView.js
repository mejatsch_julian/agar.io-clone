function getPlayerScore(player) {
    return player.entities.reduce((old, entity) => old + Math.pow(entity.r, 2) * Math.PI, 0)
}



export default function SVGView(gameState, playerId, playerSplitCallback, imageCallback, updatePlayerDirectionCallback) {
    this.gameState = gameState;
    this.playerId = playerId;
    this.imageCallback = imageCallback;
    this.updatePlayerDirectionCallback = updatePlayerDirectionCallback;


    this.init = () => {
        this.svg = document.getElementById("main-svg");
        this.svgContainer = document.getElementById("svg-container");
        this.patternCircle = document.getElementById("pattern-circle");
        this.foodGroup = document.getElementById("food-group");
        this.playerGroup = document.getElementById("player-group");
        this.leaderboardContainer = document.getElementById("leaderboard-container");
        this.leaderboardContainer.style.display = "block";
        this.textTester = document.getElementById("text-tester");
        this.imageDefs = document.getElementById("image-defs");


        this.aspectRatio;
        this.viewBoxCache = { x: -100, y: -100, width: 100, height: 100 };
        this.mousePosition = { x: 0, y: 0 };
        this.ingameMousePosition = { x: 0, y: 0 };
        this.mainPlayer = this.gameState.players.find(p => p.id == playerId);
        this.worldRadius;
        this.foodCollection;
        this.lastFoodCache = [];
        this.playerSplitCallback = playerSplitCallback;
        this.textRatioCache = {};
        this.deathRegistered = false;
        this.imageCache = {};

        this.updateAspectRatio();
        document.body.addEventListener("resize", this.updateAspectRatio);
        this.svg.addEventListener("mousemove", this.updateMousePosition);
        this.frameRenderFlags = {

        };

        document.body.addEventListener("keydown", e => {
            if (e.key == " ") {
                console.log("SPLIT");
                this.playerSplitCallback();
            }
        });


        this.patternCircle.setAttribute("r", this.gameState.worldRadius);



    }

    this.getTextRatio = (text) => {
        if (!Object.hasOwn(this.textRatioCache, text)) {
            let t = document.createElementNS("http://www.w3.org/2000/svg", "text");
            t.textContent = text;
            t.style.fontFamily = "sans-serif";

            this.textTester.appendChild(t);

            let bbox = t.getBBox();
            this.textRatioCache[text] = bbox.height / bbox.width;

            t.remove();
        }


        return this.textRatioCache[text];
    }

    this.updateAspectRatio = () => {
        this.aspectRatio =
            this.svgContainer.getBoundingClientRect().width /
            this.svgContainer.getBoundingClientRect().height;


    }

    this.updateIngameMousePosition = () => {
        this.ingameMousePosition.x =
            (this.mousePosition.x / this.svg.getBoundingClientRect().width) * this.viewBoxCache.width +
            (this.viewBoxCache.x + this.viewBoxCache.width / 2) -
            this.viewBoxCache.width / 2;

        this.ingameMousePosition.y =
            (this.mousePosition.y / this.svg.getBoundingClientRect().height) *
            this.viewBoxCache.height +
            (this.viewBoxCache.y + this.viewBoxCache.height / 2) -
            this.viewBoxCache.height / 2;
    }


    this.updateMousePosition = (e) => {

        this.mousePosition = {
            x: e.clientX,
            y: e.clientY
        };


    }


    this.syncViewBox = () => {
        this.svg.setAttribute("viewBox", `${this.viewBoxCache.x} ${this.viewBoxCache.y} ${this.viewBoxCache.width} ${this.viewBoxCache.height}`);
    }

    this.updatePlayerDirection = () => {
        this.updatePlayerDirectionCallback(this.ingameMousePosition);
    }


    this.adjustImageToRadius = (id, radius) => {
        if (this.imageCache[id] === null) return false;
        if (!Object.hasOwn(this.imageCache, id)) {
            let imageURL = this.imageCallback(id);

            if (!imageURL) return false;

            let imgPattern = document.createElementNS("http://www.w3.org/2000/svg", "pattern");
            imgPattern.setAttribute("id", `profileImage${id}`);
            imgPattern.setAttribute("patternUnits", "objectBoundingBox");

            let imgEle = document.createElementNS("http://www.w3.org/2000/svg", "image");
            imgEle.setAttribute("href", imageURL);
            imgEle.setAttribute("x", 0);
            imgEle.setAttribute("y", 0);

            imgPattern.appendChild(imgEle);

            this.imageDefs.appendChild(imgPattern);

            this.imageCache[id] = {
                imageURL: imageURL,
                imgPattern: imgPattern,
                imageEle: imgEle
            }
        }

        this.imageCache[id].imageEle.setAttribute("width", 2 * radius);
        this.imageCache[id].imageEle.setAttribute("height", 2 * radius);
        this.imageCache[id].imgPattern.setAttribute("width", 2 * radius);
        this.imageCache[id].imgPattern.setAttribute("height", 2 * radius);

        return true;
    }

    this.updateView = () => {


        this.updateIngameMousePosition();
        this.updatePlayerDirection();

        let x1 = Number.POSITIVE_INFINITY,
            x2 = Number.NEGATIVE_INFINITY;
        let y1 = Number.POSITIVE_INFINITY,
            y2 = Number.NEGATIVE_INFINITY;

        this.mainPlayer = this.gameState.players.find(p => p.id == playerId);
        if ((!this.mainPlayer || this.mainPlayer.isDead) && !this.deathRegistered) {
            this.deathRegistered = true;
            alert("YOU DIED!");


            window.location.reload();

        }


        for (const entity of this.mainPlayer.entities) {
            x1 = Math.min(x1, entity.x - entity.r);
            x2 = Math.max(x2, entity.x + entity.r);

            y1 = Math.min(y1, entity.y - entity.r);
            y2 = Math.max(y2, entity.y + entity.r);
        }




        let centerX = (x1 + x2) / 2;
        let centerY = (y1 + y2) / 2;


        let w = (x2 - x1) * (Math.exp(-0.01 * (x2 - x1) + Math.log(10)) + 5);
        let h = (y2 - y1) * (Math.exp(-0.01 * (y2 - y1) + Math.log(10)) + 5);

        if (h * this.aspectRatio < w) {
            h = w / this.aspectRatio;
        } else {
            w = h * this.aspectRatio;
        }

        this.viewBoxCache = {
            x: centerX - w / 2,
            y: centerY - h / 2,
            width: w,
            height: h
        };


        this.syncViewBox();

        this.playerGroup.textContent = "";

        let toAppend = [];
        for (const player of this.gameState.players) {
            for (const entity of player.entities) {
                let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");

                circle.setAttribute("cx", entity.x);
                circle.setAttribute("cy", entity.y);
                circle.setAttribute("stroke", "red");
                circle.setAttribute("stroke-width", "3");


                if (this.adjustImageToRadius(player.id, entity.r)) {
                    circle.setAttribute("fill", `url(#profileImage${this.playerId})`);
                } else {
                    circle.setAttribute("fill", player.fill);
                }

                circle.setAttribute("r", entity.r);
                circle.classList.add("some-player");


                let nameText = document.createElementNS("http://www.w3.org/2000/svg", "text");
                nameText.setAttribute("x", entity.x);
                nameText.setAttribute("y", entity.y);
                nameText.textContent = player.name;

                nameText.setAttribute("dominant-baseline", "middle");
                nameText.setAttribute("text-anchor", "middle");
                nameText.setAttribute('textLength', entity.r * 2 * 0.8);
                nameText.setAttribute('lengthAdjust', 'spacingAndGlyphs');
                nameText.setAttribute('font-size', entity.r * 2 * 0.8 * this.getTextRatio(player.name) + "px");


                toAppend.push([circle, nameText]);
            }
        }

        toAppend.sort((a, b) => Number(a[0].getAttribute("r")) - Number(b[0].getAttribute("r")));
        for (let ta of toAppend) {
            this.playerGroup.appendChild(ta[0]);
            this.playerGroup.appendChild(ta[1]);
        }


        for (const food of this.gameState.foodCollection.addQueue) {
            let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");

            circle.setAttribute("cx", food.x);
            circle.setAttribute("cy", food.y);
            circle.setAttribute("fill", "blue");
            circle.setAttribute("r", 40);

            this.foodGroup.appendChild(circle);
        }
        this.gameState.foodCollection.addQueue = [];

        for (const food of this.gameState.foodCollection.removeQueue) {



            this.foodGroup.querySelector(`circle[cx="${food.x}"][cy="${food.y}"]`).remove();
        }
        this.gameState.foodCollection.removeQueue = [];


        this.lastFoodCache = this.currentFoodCache;


        let sortedPlayers = this.gameState.players.filter(p => !p.isDead).toSorted((a, b) => getPlayerScore(b) - getPlayerScore(a)).slice(0, 5);
        this.leaderboardContainer.innerHTML = sortedPlayers.reduce((old, p) => old + "<li>" + `${p.name}: ${getPlayerScore(p).toFixed(0)}` + "</li>", "");
    }

    this.init();
}
