export default function BotController(gameState) {
    this.gameState = gameState;

    this.step = () => {
        let bots = this.gameState.players.filter(p => p.isBot);

        for (let bot of bots) {
            if (bot.mousePosition.x == null || bot.entities.some(entity => Math.pow(entity.x - bot.mousePosition.x, 2) + Math.pow(entity.y - bot.mousePosition.y, 2) <= Math.pow(entity.r + 10, 2))) {
                let angle = Math.random() * Math.PI * 2; // Random angle
                let distance = Math.sqrt(Math.random()) * this.gameState.worldRadius; // Square root for even distribution

                let x = distance * Math.cos(angle);
                let y = distance * Math.sin(angle);

                bot.mousePosition = {
                    x: x, y: y
                };
            }

        }
    }
}