import Game from "./Game.js";
import FoodCollectionView from "./FoodCollectionView.js";
import SVGView from "./SVGView.js";


export default function SingleplayerGame(gameStartInfo) {

    const game = new Game({ worldRadius: 2000, playerCount: 5, foodCount: 50 }, () => {
        let peerUpdate = game.popPeerUpdate();
        gameState.players = peerUpdate.players;
        gameState.foodCollection.addQueue.push(...peerUpdate.addedFood);
        gameState.foodCollection.removeQueue.push(...peerUpdate.removedFood);
    });

    const gameState = game.getSynchronizedGameState();

    gameState.foodCollection = new FoodCollectionView(gameState.foodCollection);

    let playerId = game.joinPlayer(gameStartInfo.name);

    if (gameStartInfo.imageURL)
        game.setPlayerImage(playerId, gameStartInfo.imageURL);

    const view = new SVGView(gameState, playerId, () => {
        game.splitPlayer(playerId);
    }, (id) => {

        if (game.getPlayerImage(id))

            return game.getPlayerImage(id);
    },
        (mousePos) => {

            gameState.players.find(p => p.id == playerId).mousePosition = mousePos;
            game.setPlayerMousePosition(playerId, mousePos);
        });


    window.requestAnimationFrame(function viewStep(timestamp) {
        gameState.players = game.getPlayersCopy();
        view.updateView();
        window.requestAnimationFrame(viewStep);
    });
}

SingleplayerGame.prototype.stop = function () {

}