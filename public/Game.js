import FoodCollection from "./FoodCollection.js";
import BotController from "./BotController.js";

export default function Game(gameConfig, stepCallback = null) {
    this.init = () => {
        this.playerCount = gameConfig.playerCount;
        this.stepCallback = stepCallback;
        this.gameState = {
            players: [],
            worldRadius: gameConfig.worldRadius,
            foodCollection: new FoodCollection(300, gameConfig.worldRadius, gameConfig.foodCount)
        };

        this.playerIdCounter = 1;

        for (let i = 0; i < gameConfig.playerCount; i++) {
            this.addBot();
        }

        this.botController = new BotController(this.gameState);

        this.peerUpdate = { addedFood: [], removedFood: [] };

        this.synchronizedGameState = this.getStringifiableGameState();
        this.gameState.foodCollection.addQueue = [];
        this.gameState.foodCollection.removeQueue = [];

        this.playerImages = {};

        this.startGameLoop();
    }

    this.setPlayerImage = (id, img) => {
        this.playerImages[id] = img;
    }

    this.getPlayerImage = (id) => {
        return this.playerImages[id] ?? null;
    }

    this.setPlayerMousePosition = (id, mousePosition) => {
        this.gameState.players.find(p => p.id == id).mousePosition = mousePosition;
    }

    this.getSplitForceByRadius = (radius) => {
        return 10;
    }

    this.splitPlayer = (id) => {
        let player = this.gameState.players.find(p => p.id == id);
        let newEntities = [];
        for (let entity of player.entities) {
            if (Math.pow(entity.r, 2) * Math.PI < 200) continue;

            const newR = Math.sqrt((Math.pow(entity.r, 2) * Math.PI / 2) / Math.PI);
            entity.r = newR;
            entity.life = 0;

            let dx = player.mousePosition.x - entity.x;
            let dy = player.mousePosition.y - entity.y;


            let l = Math.sqrt(dx * dx + dy * dy);
            if (l == 0) {
                dx = 0;
                dy = 0;
            } else {
                dx /= l;
                dy /= l;
            }

            newEntities.push({
                x: entity.x + dx * newR,
                y: entity.y + dy * newR,
                r: newR,
                permeable: true,
                inertia: {
                    x: dx,
                    y: dy,
                    force: this.getSplitForceByRadius(newR)
                },
                life: 0
            });
        }

        player.entities.push(...newEntities);

    }

    this.addBot = () => {
        console.log("ADD BOT");
        let radius = 100;

        let angle = Math.random() * Math.PI * 2; // Random angle
        let distance = Math.sqrt(Math.random()) * (this.gameState.worldRadius - radius); // Square root for even distribution

        let x = distance * Math.cos(angle);
        let y = distance * Math.sin(angle);

        this.gameState.players.push({
            name: `Bot${this.playerIdCounter}`,
            fill: "green",
            id: this.playerIdCounter,
            entities: [
                { x: x, y: y, r: radius, inertia: null, permeable: false, life: 0 }
            ],
            isBot: true,
            isDead: false,
            mousePosition: { x: null, y: null },
            speedModifier: 1
        });

        this.playerIdCounter++;
    }

    this.joinPlayer = (name) => {
        let radius = 100;

        let angle = Math.random() * Math.PI * 2; // Random angle
        let distance = Math.sqrt(Math.random()) * (this.gameState.worldRadius - radius); // Square root for even distribution

        let x = distance * Math.cos(angle);
        let y = distance * Math.sin(angle);

        this.gameState.players.push({
            name: name,
            fill: "orange",
            id: this.playerIdCounter,
            entities: [
                { x: x, y: y, r: radius, inertia: null, permeable: false, life: 0 }
            ],
            isBot: false,
            isDead: false,
            mousePosition: { x: 1, y: 0 },
            speedModifier: 20
        });

        this.playerIdCounter++;
        return this.playerIdCounter - 1;
    }

    this.getRadiusSpeed = (radius) => {
        const speedDrop = 0.001;
        const baseSpeed = 0.05;
        return baseSpeed * Math.exp(-speedDrop * radius) + 0.01;
    }

    this.getPlayersCopy = () => {
        return JSON.parse(JSON.stringify(this.gameState.players));
    }

    this.startGameLoop = () => {
        this.previousTimeStamp;

        window.requestAnimationFrame(this.step);
    }

    this.getStringifiableGameState = () => {
        let copiedGameState = {};
        copiedGameState.worldRadius = this.gameState.worldRadius;
        copiedGameState.players = JSON.parse(JSON.stringify(this.gameState.players));
        copiedGameState.foodCollection = this.gameState.foodCollection.getFoodCollectionData();

        return copiedGameState;
    }

    this.getSynchronizedGameState = () => {
        return this.synchronizedGameState;
    }

    this.popPeerUpdate = () => {
        let oldPeerUpdate = this.peerUpdate;
        this.peerUpdate = {};
        let result = {
            players: oldPeerUpdate.players,
            addedFood: this.gameState.foodCollection.addQueue,
            removedFood: this.gameState.foodCollection.removeQueue
        };
        this.gameState.foodCollection.addQueue = [];
        this.gameState.foodCollection.removeQueue = [];

        this.synchronizedGameState = this.getStringifiableGameState();

        return result;
    }

    this.resolveCollisions = (player) => {
        // Resolve collisions
        let touching = true;

        while (touching) {
            touching = false;

            for (let entity of player.entities) {

                if (
                    Math.pow(entity.x, 2) + Math.pow(entity.y, 2) >
                    Math.pow(this.gameState.worldRadius - entity.r, 2)
                ) {
                    touching = true;
                    let l = Math.sqrt(Math.pow(entity.x, 2) + Math.pow(entity.y, 2));
                    entity.x /= l;
                    entity.y /= l;
                    entity.x *= this.gameState.worldRadius - entity.r - 0.000001;
                    entity.y *= this.gameState.worldRadius - entity.r - 0.000001;
                }


                for (let otherEntity of player.entities) {
                    if (entity === otherEntity) continue;
                    //if (entity.permeable || otherEntity.permeable) continue;

                    if (
                        Math.pow(entity.x - otherEntity.x, 2) +
                        Math.pow(entity.y - otherEntity.y, 2) <
                        Math.pow(entity.r + otherEntity.r, 2)
                    ) {

                        touching = true;
                        let p =
                            entity.r +
                            otherEntity.r -
                            Math.sqrt(
                                Math.pow(entity.x - otherEntity.x, 2) +
                                Math.pow(entity.y - otherEntity.y, 2)
                            );

                        p += 0.00000001;

                        let dir = {
                            x: otherEntity.x - entity.x,
                            y: otherEntity.y - entity.y
                        };

                        let dirL = Math.sqrt(Math.pow(dir.x, 2) + Math.pow(dir.y, 2));

                        dir.x /= dirL;
                        dir.y /= dirL;

                        entity.x -= (dir.x * p) / 2;
                        entity.y -= (dir.y * p) / 2;

                        otherEntity.x += (dir.x * p) / 2;
                        otherEntity.y += (dir.y * p) / 2;
                    }
                }
            }

        }
    }


    this.step = (timestamp) => {
        if (this.previousTimeStamp === undefined) {
            this.previousTimeStamp = timestamp;
            window.requestAnimationFrame(this.step);
            return;
        }

        this.elapsed = timestamp - this.previousTimeStamp; // time since last frame
        this.previousTimeStamp = timestamp; // update previous timestamp

        for (let player of this.gameState.players) {
            let mergedEntities = [];
            for (let entity of player.entities) {

                let dx = player.mousePosition.x - entity.x;
                let dy = player.mousePosition.y - entity.y;


                let l = Math.sqrt(dx * dx + dy * dy);
                if (l == 0) {
                    dx = 0;
                    dy = 0;
                } else {
                    dx /= l;
                    dy /= l;
                }

                let proximityModifier = { x: 1, y: 1 }

                if (l <= entity.r) {
                    proximityModifier.x *= l / entity.r;
                    proximityModifier.y *= l / entity.r;
                }


                entity.x += dx * this.getRadiusSpeed(entity.r) * this.elapsed * player.speedModifier * proximityModifier.x;
                entity.y += dy * this.getRadiusSpeed(entity.r) * this.elapsed * player.speedModifier * proximityModifier.y;
                if (entity.inertia) {
                    entity.x += entity.inertia.x * entity.inertia.force;
                    entity.y += entity.inertia.y * entity.inertia.force;
                    entity.inertia.force -= 1;
                    if (entity.inertia.force <= 0) {
                        entity.inertia = null;
                    }
                }


                let eaten = this.gameState.foodCollection.popCircleFood(entity.x, entity.y, entity.r);


                for (let e of eaten) {
                    entity.r += .1;
                }


                for (let otherPlayer of this.gameState.players) {
                    if (player.id == otherPlayer.id) continue;


                    let toRemove = [];
                    for (let otherEntity of otherPlayer.entities) {

                        if (entity.r > otherEntity.r * 1.1 && Math.pow(entity.x - otherEntity.x, 2) + Math.pow(entity.y - otherEntity.y, 2) <= Math.pow(entity.r - otherEntity.r, 2)) {

                            entity.r = Math.sqrt((Math.pow(entity.r, 2) * Math.PI + Math.pow(otherEntity.r, 2) * Math.PI) / Math.PI);
                            toRemove.push(otherEntity);

                        }
                    }

                    for (let otherEntity of toRemove) {
                        otherPlayer.entities = otherPlayer.entities.filter(e_ => e_ != otherEntity);
                        if (otherPlayer.entities.length == 0) {
                            otherPlayer.isDead = true;
                        }
                    }
                }

                if (entity.life > 5000) {
                    for (let otherEntity of player.entities) {
                        if (entity === otherEntity) continue;
                        if (mergedEntities.includes(entity) || mergedEntities.includes(otherEntity)) continue;
                        if (entity.r > otherEntity.r) {
                            if (Math.pow(entity.x - otherEntity.x, 2) +
                                Math.pow(entity.y - otherEntity.y, 2) <
                                Math.pow(entity.r + otherEntity.r + 0.5, 2)) {

                                entity.r = Math.sqrt((Math.pow(entity.r, 2) * Math.PI + Math.pow(otherEntity.r, 2) * Math.PI) / Math.PI);

                                mergedEntities.push(otherEntity);
                            }
                        } else {
                            if (Math.pow(otherEntity.x - entity.x, 2) +
                                Math.pow(otherEntity.y - entity.y, 2) <
                                Math.pow(otherEntity.r + entity.r + 0.5, 2)) {

                                otherEntity.r = Math.sqrt((Math.pow(otherEntity.r, 2) * Math.PI + Math.pow(entity.r, 2) * Math.PI) / Math.PI);

                                mergedEntities.push(entity);
                            }
                        }



                    }
                }


                entity.life += this.elapsed;

            }

            player.entities = player.entities.filter(entity => !mergedEntities.includes(entity));


            this.resolveCollisions(player);




        }


        while (this.gameState.players.filter(p => !p.isDead).length < this.playerCount) {
            this.addBot();

        }

        this.botController.step();


        this.peerUpdate.players = this.gameState.players;

        if (this.stepCallback) this.stepCallback();
        window.requestAnimationFrame(this.step);
    }

    this.init();
}