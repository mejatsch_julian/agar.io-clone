import { Peer } from "https://esm.sh/peerjs@1.5.2?bundle-deps";

const FOOD_RADIUS = 40;

export default function Menu(playSingleplayerCallback, playMultiplayerCallback, joinPartyCallback) {
    let startSingleplayerBtn = document.getElementById("start-singleplayer");
    let createPartyBtn = document.getElementById("create-party");
    let joinPartyBtn = document.getElementById("join-party");
    let welcomeContainer = document.getElementById("welcome-container");
    let partyIdEle = document.getElementById("party-id");
    let playerNameInput = document.getElementById("player-name");
    let chooseImageBtn = document.getElementById("choose-image-btn");
    let imageURL;

    chooseImageBtn.addEventListener("click", e => {
        let fileEle = document.createElement("input");
        fileEle.type = "file";
        fileEle.accept = "image/*";
        fileEle.click();

        fileEle.oninput = e => {
            if (fileEle.files.length == 1) {
                const reader = new FileReader();
                reader.onload = function (e) {
                    const imgElement = document.createElement("img");
                    imgElement.src = e.target.result;
                    imgElement.style.display = 'block';
                    imgElement.style.width = "50px";
                    imgElement.style.width = "50px";

                    imageURL = e.target.result;

                    chooseImageBtn.innerText = ":";
                    chooseImageBtn.appendChild(imgElement);
                }
                reader.readAsDataURL(fileEle.files[0]);
            }
        }
    });




    startSingleplayerBtn.addEventListener("click", e => {
        welcomeContainer.style.display = "none";
        playSingleplayerCallback({
            name: playerNameInput.value,
            imageURL: imageURL
        });


    });

    createPartyBtn.addEventListener("click", e => {
        welcomeContainer.style.display = "none";
        playMultiplayerCallback();

    });



    joinPartyBtn.addEventListener("click", e => {
        welcomeContainer.style.display = "none";

        joinPartyCallback();



    });
}
