


export default function MultiplayerGame() {
    const game = new Game({ worldRadius: 20000, playerCount: 200, foodCount: 10000 }, () => {
        let peerUpdate = game.popPeerUpdate();
        gameState.players = peerUpdate.players;
        gameState.foodCollection.addQueue.push(...peerUpdate.addedFood);
        gameState.foodCollection.removeQueue.push(...peerUpdate.removedFood);

        // Now to the server stuff
        for (let conn of connections) {
            conn.send({
                type: "peer_update",
                data: peerUpdate
            });
        }
    });



    const gameState = game.getSynchronizedGameState();

    gameState.foodCollection = new FoodCollectionView(gameState.foodCollection);

    let playerId = game.joinPlayer(playerNameInput.value);

    if (imageURL)
        game.setPlayerImage(playerId, imageURL);

    const view = new SVGView(gameState, playerId, () => {
        game.splitPlayer(playerId);
    }, (id) => {

        if (game.getPlayerImage(id))

            return game.getPlayerImage(id);
    });

    window.requestAnimationFrame(function viewStep(timestamp) {
        gameState.players = game.getPlayersCopy();
        game.setPlayerMousePosition(playerId, view.ingameMousePosition);
        view.updateView();
        window.requestAnimationFrame(viewStep);
    });


    let peer = new Peer();
    let connections = [];
    peer.on("open", (id) => {
        partyIdEle.innerText = id;
        partyIdEle.style.display = "block";
        navigator.clipboard.writeText(id);

        peer.on("connection", (conn) => {
            connections.push(conn);

            conn.on("data", (response) => {

                if (response.type == "get_player_picture") {
                    conn.send({
                        type: "player_picture_reply",
                        data: {
                            imageURL: game.getPlayerImage(response.data.playerId)
                        }
                    });
                }


                if (response.type == "join_request") {
                    console.log("JOIN REQEUSTED")
                    conn.send({
                        type: "join_reply",
                        joinDenied: false,
                        data: {
                            initialData: game.getSynchronizedGameState(),
                            playerId: game.joinPlayer()
                        }
                    });

                }

                if (response.type == "mouse_position_update") {
                    game.setPlayerMousePosition(response.playerId, response.position);
                }


            });
        });
    });



}

MultiplayerGame.prototype.stop = function () {

}