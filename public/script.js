import Menu from "./Menu.js";
import SingleplayerGame from "./SingleplayerGame.js";
import MultiplayerGame from "./MultiplayerGame.js";
import JoinParty from "./JoinParty.js";

function playSingleplayer(gameStartInfo) {
    let game = new SingleplayerGame(gameStartInfo);
}

function playMultiplayer(gameStartInfo) {
    let game = new MultiplayerGame(gameStartInfo);
}

function joinParty(gameStartInfo) {
    let game = new JoinParty(gameStartInfo);
}

const menu = new Menu(playSingleplayer, playMultiplayer, joinParty);





