const FOOD_RADIUS = 40;

export default function FoodCollection(cellSize, worldRadius, goalNumber) {
    this.map = new Map();
    this.cellSize = cellSize;
    this.worldRadius = worldRadius;
    this.addQueue = [];
    this.removeQueue = [];
    this.goalNumber = goalNumber;

    this.insertFood = (x, y) => {
        let cellX = Math.floor(x / cellSize);
        let cellY = Math.floor(y / cellSize);

        let arr = this.map.get(JSON.stringify({ x: cellX, y: cellY }));
        if (arr) arr.push({ x: x, y: y });
        else this.map.set(JSON.stringify({ x: cellX, y: cellY }), [{ x: x, y: y }]);

        this.addQueue.push({ x: x, y: y });
    }

    this.removeFood = (x, y) => {
        let cellX = Math.floor(x / cellSize);
        let cellY = Math.floor(y / cellSize);

        let arr = this.map.get(JSON.stringify({ x: cellX, y: cellY }));
        this.map.set(JSON.stringify({ x: cellX, y: cellY }), arr.filter(p => !(p.x == x && p.y == y)));
        this.removeQueue.push({ x: x, y: y });
        this.addNewRandomFood();
    }

    this.popCircleFood = (x, y, r) => {
        let x1 = x - r;
        let x2 = x + r;
        let y1 = y - r;
        let y2 = y + r;

        let cellX1 = Math.floor(x1 / cellSize);
        let cellY1 = Math.floor(y1 / cellSize);
        let cellX2 = Math.ceil(x2 / cellSize);
        let cellY2 = Math.ceil(y2 / cellSize);

        let toRemove = [];
        for (let cx = cellX1; cx <= cellX2; cx++) {
            for (let cy = cellY1; cy <= cellY2; cy++) {

                let arr = this.map.get(JSON.stringify({ x: cx, y: cy }));

                if (!arr) continue;



                for (let p of arr) {
                    if (Math.pow(p.x - x, 2) + Math.pow(p.y - y, 2) <= Math.pow(r, 2)) {
                        toRemove.push(p);
                    }
                }
            }
        }

        for (let p of toRemove) {
            this.removeFood(p.x, p.y);
        }

        return toRemove;
    }

    this.getAllFood = () => {
        let result = [];
        Array.from(this.map.values()).map(list => { list.map(p => { result.push(p) }) });

        return result;
    }

    this.getFoodCollectionData = () => {
        return { food: this.getAllFood() };
    }

    this.addNewRandomFood = () => {
        let angle = Math.random() * Math.PI * 2; // Random angle
        let distance = Math.sqrt(Math.random()) * this.worldRadius - FOOD_RADIUS; // Square root for even distribution

        let x = distance * Math.cos(angle);
        let y = distance * Math.sin(angle);


        this.insertFood(x, y);
    }

    for (let i = 0; i < goalNumber; i++) {
        this.addNewRandomFood();
    }
}
